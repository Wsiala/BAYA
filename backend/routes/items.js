const router = require('express').Router();
let Item = require('../models/items.model');
const multer = require('../middleware/multer-config');
const fs = require('fs');

var imageUrl;

router.get('/', (req, res) => {
  Item.find()
  .then(items => res.json(items))
  .catch(err => res.status(400).json('Error: ' + err));

})

router.post('/upload', multer, (req, res) => {
  imageUrl = `${req.protocol}://${req.get('host')}/uploads/${req.file.filename}`;
  res.json({
    'message' : 'Files uploaded sucss.'
  })
})

router.post('/add', multer, (req, res) => {
  const name = req.body.name;
  const description = req.body.description;
  const image = imageUrl;
  const newItem = new Item({name: name, description: description, image: image });

  newItem.save().then(post => {
    if(post) {
      res.json(post)
    }
  })
  .catch(err => res.status(400).json({err}))
})

router.post('/delete', (req, res) => {
  Item.findById(req.body.id)
    .then(item => {
      const itemName = item.image.split('/uploads/')[1];
      fs.unlink(`uploads/${itemName}`, () => {
        Item.findByIdAndDelete(req.body.id)
          .then(() => res.status(200).json({message: 'Objet supprimé'}))
          .catch(error => err.status(400).json({error}))
      })
    })
    .catch(error => res.status(500).json({error}))
})

module.exports = router;
