const router = require('express').Router();
const bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
let User = require('../models/users.model');

router.route('/').get((req, res) => {
  User.find()
  .then(users => res.json(users))
  .catch(err => res.status(400).json('Error: ' + err));
});

router.post('/add', (req, res) => {
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password
  const hash = bcrypt.hashSync(password, 10);

  const newUser = new User({username: username, email: email, password: hash});

  newUser.save().then(post => {
    if (post) {
      res.json({_id : post.id, email : post.email, username : post.username})
    }
  }).catch(error => {
    if(error.keyValue){
      res.json({error: error.keyValue})
    } else {
      res.json({error: 'problème de serveur'})
    }
  })
});

router.post('/connect', async (req, res) => {
  const { email, password } = req.body;
  try {
    let user = await User.findOne({ email });
    if (!user) {
      return res
          .status(401)
          .json({ success: false, error: 'user_not_found'});
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.json({ success: false, error: 'Invalid password'})
    } else {
      const userConnect = {
        _id: user._id,
        name: user.name,
        email: user.email,
      };
      let privateKey = process.env.jwtSecret;
      const token = jwt.sign(userConnect, privateKey, { expiresIn: 360000});
      res.json({
        success: true,
        token: token,
        user: userConnect,
      });
    }
  }
  catch(err) {
    console.error(err.message);
    res.status(500).send('Serveur erreurs');
  }
});

module.exports = router;
