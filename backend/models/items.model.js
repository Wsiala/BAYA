const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const itemSchema = new Schema ({
  name: {
    type: String,
    require: true,
    trim: true
  },
  description: {
    type: String,
    require: true
  },
  image: {
    type: String,
  }
});

const Item = mongoose.model('Item', itemSchema);

module.exports = Item;
