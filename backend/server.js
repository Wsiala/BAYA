const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

app.use(bodyParser.json())
app.use('/uploads', express.static(path.join(__dirname, 'uploads')))

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})

const usersRouter = require('./routes/users');
const itemsRouter = require('./routes/items');

app.use('/users', usersRouter);
app.use('/items', itemsRouter);

app.listen(port, () => {
  console.log(`server is running on port: ${port}`);
});
