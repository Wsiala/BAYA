import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogEchangeComponent } from './blog-echange.component';

describe('BlogEchangeComponent', () => {
  let component: BlogEchangeComponent;
  let fixture: ComponentFixture<BlogEchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogEchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogEchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
