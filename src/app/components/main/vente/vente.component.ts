import { Component, OnInit } from '@angular/core';
import { FormBuilder, NgForm, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ItemsService } from '../../services/items.service';


@Component({
  selector: 'app-vente',
  templateUrl: './vente.component.html',
  styleUrls: ['./vente.component.css']
})
export class VenteComponent implements OnInit {

  addNewItemForm;
  imageDownloaded = false;
  imageFileDownload: File = null;
  itemsList;
  itemId;
  imageDownloadName;
  formData;

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private itemService: ItemsService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.addNewItemForm = this.formBuilder.group({
      'name': [null, [Validators.required]],
      'description': [null, Validators.required],
    })
    this.getAllItems();
  }

  getAllItems() {
    this.itemsList = null;
    this.itemService.getAllItems().then(res => {
      if (res) {
        this.itemsList = res;
      }
    })
  }

  openModalNewItem(content) {
    this.addNewItemForm.reset();
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      windowClass: 'modal-window',
      backdropClass: 'modal-background',
    });
  }
  openModalDeleteItem(content, itemId) {
    this.itemId = itemId
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      backdropClass: 'modal-background',
    })
  }
  closeModal() {
    this.modalService.dismissAll();
  }
  onFileChanged(files) {
    this.imageFileDownload = files.item(0);

    if(this.imageFileDownload['type'].includes('image')) {
      this.imageDownloaded = true;
      this.imageDownloadName = this.imageFileDownload['name']
      this.formData = new FormData();
      this.formData.append('image', this.imageFileDownload, this.imageDownloadName);
    } else {
      this.imageDownloaded = false;
    }
  }
  onAddNewItemSubmit(form: NgForm) {
    if(this.imageDownloaded) {
      let item = {
        name: form["name"],
        description: form["description"],
      }
      this.itemService.file(this.formData).then();
      this.itemService.addNewItem(item).then(res => {
        if (res) {
          this.getAllItems();
          this.closeModal();
        }
      });
    }
  }
  deleteItem() {
    this.itemService.deleteItem(this.itemId).then(res => {
      if (res) {
        this.getAllItems();
        this.closeModal();
      }
    })
  }
}
