import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  category;

  constructor(
    public router: Router,
  ) { }

  ngOnInit(): void {
  }

  category_selected(cat) {
    if (cat == 'vente') {
      this.category = 'vente';
      this.router.navigate(['/vente'])
    } else {
      this.category = 'blog';
      this.router.navigate(['/blog d\'échange'])
    }

  }

  redirectHome() {
    this.router.navigate(['/home'])
  }
}
