import { Component, OnInit } from '@angular/core';
import { FormBuilder, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signUpUserForm;
  createUserForm;
  userMember = true;
  errorEmail = false;
  errorServer = false;
  invalidData = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.signUpUserForm = this.formBuilder.group({
      'email': [null, [Validators.required, Validators.email]],
      'password': [null, Validators.required],
    })
    this.createUserForm = this.formBuilder.group({
      'email': [null, [Validators.required, Validators.email]],
      'password': [null, Validators.required],
      'name': [null, Validators.required],
    })
  }

  signUpUser() {
    this.userMember = false;
  }
  connexion() {
    this.userMember = true;
  }
  onUsersignUpSubmit(form: NgForm) {
    let user = {
      email: form["email"],
      password: form["password"]
    }
    this.authService.login(user).then(res => {
      if(res['error']) {
        this.invalidData = true;
      } else {
        this.router.navigate(['/home'])
      }
    })
  }

  onUserCreateSubmit(form: NgForm) {
    let user = {
      username: form["name"],
      email: form["email"],
      password: form["password"]
    }

    this.authService.signUp(user).then(res => {
      if(res['error']) {
        if(res['error'].hasOwnProperty('email')) {
          this.errorEmail = true;
        } else {
          this.errorServer = true;
        }
      } else {
        this.router.navigate(['/home'])
      }
    })
  }
}
