import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  getAllItems() {
    return this.http.get('http://localhost:5000/items/').toPromise();
  }

  addNewItem(item) {
    return this.http.post('http://localhost:5000/items/add', item).toPromise();
  }

  deleteItem(id) {
    return this.http.post('http://localhost:5000/items/delete', {id}).toPromise();
  }

  file(formData) {
    return this.http.post('http://localhost:5000/items/upload', formData).toPromise();
  }
}
