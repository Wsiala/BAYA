import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userId;
  private token;

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  login(user) {
    return this.http.post<{user, token: string, success: string}>('http://localhost:5000/users/connect', user).toPromise()
    .then(res => {
      if(res.success) {
        this.token = res['token'];
        this.userId = res['user']['_id'];
        return ('connected')
      } else {
        return res;
      }
    });
  }

  signUp(user) {
    return this.http.post('http://localhost:5000/users/add', user).toPromise();
  }
}
