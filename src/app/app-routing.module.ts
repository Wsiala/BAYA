import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogEchangeComponent } from './components/main/blog-echange/blog-echange.component';
import { HomePageComponent } from './components/main/home-page/home-page.component';
import { LoginComponent } from './components/main/login/login.component';
import { MainComponent } from './components/main/main.component';
import { VenteComponent } from './components/main/vente/vente.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomePageComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'vente',
        component: VenteComponent
      },
      {
        path: 'blog d\'échange',
        component: BlogEchangeComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
